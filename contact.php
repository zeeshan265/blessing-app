<!DOCTYPE HTML>
<html lang="en">

<!-- Mirrored from www.enableds.com/products/appeca20/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 08 Oct 2019 19:24:10 GMT -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <title>Blessing</title>
    <link rel="stylesheet" type="text/css" href="styles/framework.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
</head>

<body>
    <div id="preloader" class="preloader-light">
        <h1></h1>
        <div id="preload-spinner"></div>
       <!--  <p>The Ultimate Mobile Experience</p>
        <em>This will only take a second. It's totally worth it!</em> -->
    </div>
    <div id="page-transitions">
        <div id="header" class="header-logo-left header-dark">
            <a href="#" class="header-logo"></a>
            <a href="#" class="header-icon header-icon-1 hamburger-animated" data-deploy-menu="menu-1"></a>
            <a href="#" class="header-icon header-icon-2 font-14" data-deploy-menu="menu-4"><i class="far fa-envelope"></i></a>
            <a href="#" class="header-icon header-icon-3 font-13 no-border" data-deploy-menu="menu-2"><i class="far fa-bell font-12"></i></a>
        </div>
        <div id="menu-1" class="menu-wrapper menu-light menu-sidebar-left menu-large">
            <div class="menu-scroll">
                <div class="menu-socials">
                    <a href="https://www.facebook.com/enabled.labs" class="font-12"><i class="fab facebook-color fa-facebook-f"></i></a>
                    <a href="https://twitter.com/iEnabled" class="font-12"><i class="fab twitter-color fa-twitter"></i></a>
                    <a href="https://plus.google.com/u/1/105775801838187143320" class="font-12"><i class="fab google-color fa-google-plus-g"></i></a>
                    <a href="tel:+(1234)567890" class="font-13"><i class="fa color-green-dark fa-phone"></i></a>
                    <a href="page-contact.html" class="font-13"><i class="fa color-blue2-light fa-envelope"></i></a>
                    <a href="#" class="font-14 close-menu"><i class="fa color-red-dark fa-times"></i></a>
                </div>
                <a href="index.html" class="menu-logo"></a>
                <div class="dark-mode-toggle"><a href="#" class="toggle-2 toggle-trigger"><strong><u></u>Dark Mode</strong><i class="bg-green-dark"></i><span></span><i class="bg-gray-light"></i></a></div>
                <div class="dark-menu-toggle"><a href="#" class="toggle-2 toggle-trigger"><strong><u></u>Dark Menu</strong><i class="bg-green-dark"></i><span></span><i class="bg-gray-light"></i></a></div>
                <div class="menu">
                    
                    <a class="menu-item active-item" href="dashboard.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Home</strong></a>
                    <a class="menu-item " href="sermons.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Sermon</strong></a>
                    <a class="menu-item " href="events.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Event</strong></a>
                    <a class="menu-item " href="gallery.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Gallery</strong></a>
                    <a class="menu-item " href="prayer.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Prayer Request</strong></a>
                    <a class="menu-item " href="contact.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Contact Us</strong></a>
                    <a class="menu-item close-menu " href="#"><i class="font-14 fa color-orange-dark fa-times"></i><strong>Close</strong></a>
                    <a class="menu-item close-menu " href="#"><i class="font-14 fa  fa-info-circle"></i><strong>Privacy Policy</strong></a>
                </div>
            </div>
        </div>
        <div id="page-content" class="page-content">
            <div id="page-content-scroll">
                <div class="content-fullscreen">
                    <iframe class="responsive-image maps no-bottom" src="https://maps.google.com/?ie=UTF8&amp;ll=47.595131,-122.330414&amp;spn=0.006186,0.016512&amp;t=h&amp;z=17&amp;output=embed"></iframe>
                    <a href="pageapp-map-full.html" class="button button-dark button-xs button-full uppercase bold">FullScreen Map</a>
                </div>
                <div class="content">
                    <div class="container heading-style">
                        <i class="fa fa-envelope heading-icon color-night-dark font-17"></i>
                        <h4 class="heading-title">Get in touch!</h4>
                        <!-- <p class="heading-subtitle">
                            A fully working PHP/AJAX contact form made to be as easy as possible to use.
                        </p> -->
                    </div>
                    <div class="container no-bottom">
                        <div class="contact-form no-bottom">
                            <div class="formSuccessMessageWrap" id="formSuccessMessageWrap">
                                <div class="notification-large notification-has-icon notification-green">
                                    <div class="notification-icon"><i class="fa fa-check notification-icon"></i></div>
                                    <h1 class="uppercase ultrabold">Message sent</h1>
                                    <p>We'll get back to you in the shorts possible time.</p>
                                    <a href="#" class="close-notification"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <form action="https://www.enableds.com/products/appeca20/php/contact.php" method="post" class="contactForm" id="contactForm">
                                <fieldset>
                                    <div class="formValidationError bg-red-dark" id="contactNameFieldError">
                                        <p class="center-text uppercase small-text color-white">Name is required!</p>
                                    </div>
                                    <div class="formValidationError bg-red-dark" id="contactEmailFieldError">
                                        <p class="center-text uppercase small-text color-white">Mail address required!</p>
                                    </div>
                                    <div class="formValidationError bg-red-dark" id="contactEmailFieldError2">
                                        <p class="center-text uppercase small-text color-white">Mail address must be valid!</p>
                                    </div>
                                    <div class="formValidationError bg-red-dark" id="contactMessageTextareaError">
                                        <p class="center-text uppercase small-text color-white">Message field is empty!</p>
                                    </div>
                                    <div class="formFieldWrap">
                                        <label class="field-title contactNameField" for="contactNameField">Name:<span>(required)</span>
                                        </label>
                                        <input type="text" name="contactNameField" value="" class="contactField requiredField" id="contactNameField" />
                                    </div>
                                    <div class="formFieldWrap">
                                        <label class="field-title contactEmailField" for="contactEmailField">Email: <span>(required)</span>
                                        </label>
                                        <input type="text" name="contactEmailField" value="" class="contactField requiredField requiredEmailField" id="contactEmailField" />
                                    </div>
                                    <div class="formTextareaWrap">
                                        <label class="field-title contactMessageTextarea" for="contactMessageTextarea">Message: <span>(required)</span>
                                        </label>
                                        <textarea name="contactMessageTextarea" class="contactTextarea requiredField" id="contactMessageTextarea"></textarea>
                                    </div>
                                    <div class="formSubmitButtonErrorsWrap contactFormButton">
                                        <input type="submit" class="buttonWrap button bg-blue-dark button-sm button-rounded uppercase ultrabold contactSubmitButton" id="contactSubmitButton" value="Send Message" data-formId="contactForm" />
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="decoration"></div>
                    <div class="contact-information last-column" style="text-align: center;">
                        <div class="container no-bottom ">
                            <h4>Church Information</h4>
                            
                            <p class="contact-information">
                                <strong>Minhas Camp</strong>
                                <br> PAF Complex E-9 Islamabad
                                
                            </p>
                            <p class="contact-information">
                                <strong>Contact Information:</strong>
                                <br>
                                <a href="tel: +123 456 7890"><i class="fa fa-phone-square color-green-dark"></i>+923002425265</a>
                                <a href="mailto:name@domain.com"><i class="fa fa-envelope-square color-blue-dark"></i>zeeshiq58@gmail.com</a>
                                
                            </p>
                        </div>
                    </div>
                </div>
                <div class="decoration decoration-margins"></div>
                 <div class="footer footer-dark">
                    <a href="#" class="footer-logo"><img src="images/logo-light.png" alt="" style="width: 110px;margin-left: auto;margin-right: auto;bottom: 20px;"></a>
                    <p style="margin-top: -20px;">Jesus is good news for everyone.
                        Come and meet our growing community and see what Jesus is doing right here in Minhas Camp.</p>
                    <div class="footer-socials">
                        <a href="#" class="scale-hover icon icon-round no-border icon-xs facebook-bg border-teal-3d"><i class="fab fa-facebook-f"></i></a>
                        <a href="#" class="scale-hover icon icon-round no-border icon-xs twitter-bg"><i class="fab fa-twitter"></i></a>
                        <a href="#" class="scale-hover icon icon-round no-border icon-xs google-bg"><i class="fab fa-google-plus-g"></i></a>
                        <a href="#" class="scale-hover icon icon-round no-border icon-xs phone-bg"><i class="fa fa-phone"></i></a>
                        <a href="#" data-deploy-menu="menu-share" class="scale-hover icon icon-round no-border icon-xs bg-teal-dark"><i class="fa fa-retweet font-15"></i></a>
                        <a href="#" class="scale-hover icon icon-round no-border icon-xs back-to-top bg-blue-dark"><i class="fa fa-angle-up font-16"></i></a>
                    </div>
                    <p class="copyright-text">Copyright &copy; Blessing <span id="copyright-year">2019</span>. All Rights Reserved.</p>
                </div>
            </div>
        </div>
        <a href="#" class="back-to-top-badge back-to-top-small"><i class="fa fa-angle-up"></i>Back to Top</a>
    </div>
    <script type="text/javascript" src="scripts/jquery.js"></script>
    <script type="text/javascript" src="scripts/custom.js"></script>
    <script type="text/javascript" src="scripts/plugins.js"></script>
</body>
</html>