<!DOCTYPE HTML>
<html lang="en">

<!-- Mirrored from www.enableds.com/products/appeca20/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 08 Oct 2019 19:24:10 GMT -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <title>Blessing</title>
    <link rel="stylesheet" type="text/css" href="styles/framework.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
</head>

<body>
    <div id="preloader" class="preloader-light">
        <h1></h1>
        <div id="preload-spinner"></div>
       <!--  <p>The Ultimate Mobile Experience</p>
        <em>This will only take a second. It's totally worth it!</em> -->
    </div>
    <div id="page-transitions">
        <div id="header" class="header-logo-left header-dark">
            <a href="#" class="header-logo"></a>
            <a href="#" class="header-icon header-icon-1 hamburger-animated" data-deploy-menu="menu-1"></a>
            <a href="#" class="header-icon header-icon-2 font-14" data-deploy-menu="menu-4"><i class="far fa-envelope"></i></a>
            <a href="#" class="header-icon header-icon-3 font-13 no-border" data-deploy-menu="menu-2"><i class="far fa-bell font-12"></i></a>
        </div>
        <div id="menu-1" class="menu-wrapper menu-light menu-sidebar-left menu-large">
            <div class="menu-scroll">
                <div class="menu-socials">
                    <a href="https://www.facebook.com/enabled.labs" class="font-12"><i class="fab facebook-color fa-facebook-f"></i></a>
                    <a href="https://twitter.com/iEnabled" class="font-12"><i class="fab twitter-color fa-twitter"></i></a>
                    <a href="https://plus.google.com/u/1/105775801838187143320" class="font-12"><i class="fab google-color fa-google-plus-g"></i></a>
                    <a href="tel:+(1234)567890" class="font-13"><i class="fa color-green-dark fa-phone"></i></a>
                    <a href="page-contact.html" class="font-13"><i class="fa color-blue2-light fa-envelope"></i></a>
                    <a href="#" class="font-14 close-menu"><i class="fa color-red-dark fa-times"></i></a>
                </div>
                <a href="index.html" class="menu-logo"></a>
                <div class="dark-mode-toggle"><a href="#" class="toggle-2 toggle-trigger"><strong><u></u>Dark Mode</strong><i class="bg-green-dark"></i><span></span><i class="bg-gray-light"></i></a></div>
                <div class="dark-menu-toggle"><a href="#" class="toggle-2 toggle-trigger"><strong><u></u>Dark Menu</strong><i class="bg-green-dark"></i><span></span><i class="bg-gray-light"></i></a></div>
                <div class="menu">
                    
                    <a class="menu-item active-item" href="dashboard.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Home</strong></a>
                    <a class="menu-item " href="sermons.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Sermon</strong></a>
                    <a class="menu-item " href="events.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Event</strong></a>
                    <a class="menu-item " href="gallery.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Gallery</strong></a>
                    <a class="menu-item " href="prayer.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Prayer Request</strong></a>
                    <a class="menu-item " href="contact.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Contact Us</strong></a>
                    <a class="menu-item close-menu " href="#"><i class="font-14 fa color-orange-dark fa-times"></i><strong>Close</strong></a>
                    <a class="menu-item close-menu " href="#"><i class="font-14 fa  fa-info-circle"></i><strong>Privacy Policy</strong></a>
                </div>
            </div>
        </div>