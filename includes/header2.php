<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <title>MMS-App</title>
    <link rel="stylesheet" type="text/css" href="styles/framework.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
</head>

<body>
    <div id="preloader" class="preloader-light">
        <h1></h1>
        <div id="preload-spinner"></div>
    </div>
    <div id="page-transitions">
        <div id="menu-1" class="menu-wrapper menu-light menu-sidebar-left menu-large">

            <div class="menu-scroll">
                
                <a href="index.html" class="menu-logo menu-divider"></a>
                <div class="menu">
                   
                    <a class="menu-item active-item" href="dashboard.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Home</strong></a>
                    <a class="menu-item close-menu " href="#"><i class="font-14 fa color-orange-dark fa-times"></i><strong>Close</strong></a>
                    <a class="menu-item close-menu " href="#"><i class="font-14 fa  fa-info-circle"></i><strong>Privacy Policy</strong></a>
                </div>
            </div>
        </div>

        <div id="header" class="header-logo-right header-dark">

            <a href="#" class="header-logo">
                <img src="images/logo-light.png" alt="" style="width: 110px;right: 25px;">
            </a>
            
            <a href="#" class="header-icon header-icon-1 hamburger-animated" data-deploy-menu="menu-1"></a>
            
        </div>
        