<!DOCTYPE HTML>
<html lang="en">

<!-- Mirrored from www.enableds.com/products/appeca20/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 08 Oct 2019 19:24:10 GMT -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <title>Blessing</title>
    <link rel="stylesheet" type="text/css" href="styles/framework.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
</head>

<body>
    <div id="preloader" class="preloader-light">
        <h1></h1>
        <div id="preload-spinner"></div>
       <!--  <p>The Ultimate Mobile Experience</p>
        <em>This will only take a second. It's totally worth it!</em> -->
    </div>
    <div id="page-transitions">
        <div id="header" class="header-logo-left header-dark">
            <a href="#" class="header-logo"></a>
            <a href="#" class="header-icon header-icon-1 hamburger-animated" data-deploy-menu="menu-1"></a>
            <a href="#" class="header-icon header-icon-2 font-14" data-deploy-menu="menu-4"><i class="far fa-envelope"></i></a>
            <a href="#" class="header-icon header-icon-3 font-13 no-border" data-deploy-menu="menu-2"><i class="far fa-bell font-12"></i></a>
        </div>
        <div id="menu-1" class="menu-wrapper menu-light menu-sidebar-left menu-large">
            <div class="menu-scroll">
                <div class="menu-socials">
                    <a href="https://www.facebook.com/enabled.labs" class="font-12"><i class="fab facebook-color fa-facebook-f"></i></a>
                    <a href="https://twitter.com/iEnabled" class="font-12"><i class="fab twitter-color fa-twitter"></i></a>
                    <a href="https://plus.google.com/u/1/105775801838187143320" class="font-12"><i class="fab google-color fa-google-plus-g"></i></a>
                    <a href="tel:+(1234)567890" class="font-13"><i class="fa color-green-dark fa-phone"></i></a>
                    <a href="page-contact.html" class="font-13"><i class="fa color-blue2-light fa-envelope"></i></a>
                    <a href="#" class="font-14 close-menu"><i class="fa color-red-dark fa-times"></i></a>
                </div>
                <a href="index.html" class="menu-logo"></a>
                <div class="dark-mode-toggle"><a href="#" class="toggle-2 toggle-trigger"><strong><u></u>Dark Mode</strong><i class="bg-green-dark"></i><span></span><i class="bg-gray-light"></i></a></div>
                <div class="dark-menu-toggle"><a href="#" class="toggle-2 toggle-trigger"><strong><u></u>Dark Menu</strong><i class="bg-green-dark"></i><span></span><i class="bg-gray-light"></i></a></div>
                <div class="menu">
                    
                    <a class="menu-item active-item" href="index.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Home</strong></a>
                    <a class="menu-item " href="sermons.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Sermon</strong></a>
                    <a class="menu-item " href="events.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Event</strong></a>
                    <a class="menu-item " href="gallery.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Gallery</strong></a>
                    <a class="menu-item " href="prayer.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Prayer Request</strong></a>
                    <a class="menu-item " href="contact.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Contact Us</strong></a>
                    <a class="menu-item close-menu " href="#"><i class="font-14 fa color-orange-dark fa-times"></i><strong>Close</strong></a>
                    <a class="menu-item close-menu " href="#"><i class="font-14 fa  fa-info-circle"></i><strong>Privacy Policy</strong></a>
                </div>
            </div>
        </div>
        <div id="page-content" class="page-content">
            <div id="page-content-scroll">
                <div class="single-slider owl-carousel owl-has-dots-over bg-black">
                    <div>
                        <div class="cover-content cover-content-center">
                            <h1 class="center-text color-white small-bottom">Put Your Faith Into Action </h1>
                            <p class="center-text color-white opacity-80 no-bottom">With God, you are stronger than your struggles and more fierce than your fears</p>
                        </div>
                        <div class="cover-overlay overlay "></div>
                        <img width="700" class="owl-lazy" src="images/empty.png" data-src="images/slider/bg-2.jpg" data-src-retina="images/slider/bg-2.jpg">
                    </div>
                    <div>
                        <div class="cover-content cover-content-center">
                            <h1 class="center-text color-white small-bottom">Total Surrender To God</h1>
                            <p class="center-text color-white opacity-80 no-bottom">In our abandonment we give ourselves over to God just as God gave Himself for us without any calculations</p>
                        </div>
                        
                        <div class="cover-overlay overlay "></div>
                        <img width="700" class="owl-lazy" src="images/empty.png" data-src="images/slider/bg-3.jpg" data-src-retina="images/slider/bg-3.jpg">
                    </div>
                </div>
                <div class="homepage-cta full-top full-bottom">
                    <h1 class="half-top center-text no-bottom font-21">Welcome</h1>
                    <h2 class="center-text opacity-40">It is Blessing Time.</h2>
                    <p class="boxed-text half-bottom">
                        For better or for worse, what you believe shapes your life. The more your beliefs line up with reality—the way things really are—the more you find fulfillment, purpose, and joy in the world. Our church believes the only truth that brings lasting fulfillment, clear purpose, and true joy is this: God made all people to glorify him and enjoy him forever. God made us for himself, and our hearts are restless until they rest in Him..
                    </p>
                    <div class="single-slider-no-timeout owl-carousel owl-auto-height smal-top">
                        <div>
                            <div class="homepage-cta-button">
                                <div class="button-center-large"><a href="dashboard.php" class="button button-full button-blue button-sm button-rounded uppercase ultrabold">READ MORE</a></div>
                                
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="decoration decoration-margins"></div>
                <div class="footer footer-dark">
                    <a href="#" class="footer-logo"><img src="images/logo-light.png" alt="" style="width: 110px;margin-left: auto;margin-right: auto;bottom: 20px;"></a>
                    <p style="margin-top: -20px;">Jesus is good news for everyone.
                        Come and meet our growing community and see what Jesus is doing right here in Minhas Camp.</p>
                    <div class="footer-socials">
                        <a href="#" class="scale-hover icon icon-round no-border icon-xs facebook-bg border-teal-3d"><i class="fab fa-facebook-f"></i></a>
                        <a href="#" class="scale-hover icon icon-round no-border icon-xs twitter-bg"><i class="fab fa-twitter"></i></a>
                        <a href="#" class="scale-hover icon icon-round no-border icon-xs google-bg"><i class="fab fa-google-plus-g"></i></a>
                        <a href="#" class="scale-hover icon icon-round no-border icon-xs phone-bg"><i class="fa fa-phone"></i></a>
                        <a href="#" data-deploy-menu="menu-share" class="scale-hover icon icon-round no-border icon-xs bg-teal-dark"><i class="fa fa-retweet font-15"></i></a>
                        <a href="#" class="scale-hover icon icon-round no-border icon-xs back-to-top bg-blue-dark"><i class="fa fa-angle-up font-16"></i></a>
                    </div>
                    <p class="copyright-text">Copyright &copy; Blessing <span id="copyright-year">2019</span>. All Rights Reserved.</p>
                </div>
            </div>
        </div>
        <a href="#" class="back-to-top-badge back-to-top-small"><i class="fa fa-angle-up"></i>Back to Top</a>
    </div>
    <script type="text/javascript" src="scripts/jquery.js"></script>
    <script type="text/javascript" src="scripts/custom.js"></script>
    <script type="text/javascript" src="scripts/plugins.js"></script>
</body>
</html>