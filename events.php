<!DOCTYPE HTML>
<html lang="en">

<!-- Mirrored from www.enableds.com/products/appeca20/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 08 Oct 2019 19:24:10 GMT -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <title>Blessing</title>
    <link rel="stylesheet" type="text/css" href="styles/framework.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
</head>

<body>
    <div id="preloader" class="preloader-light">
        <h1></h1>
        <div id="preload-spinner"></div>
       <!--  <p>The Ultimate Mobile Experience</p>
        <em>This will only take a second. It's totally worth it!</em> -->
    </div>
    <div id="page-transitions">
        <div id="header" class="header-logo-left header-dark">
            <a href="#" class="header-logo"></a>
            <a href="#" class="header-icon header-icon-1 hamburger-animated" data-deploy-menu="menu-1"></a>
            <a href="#" class="header-icon header-icon-2 font-14" data-deploy-menu="menu-4"><i class="far fa-envelope"></i></a>
            <a href="#" class="header-icon header-icon-3 font-13 no-border" data-deploy-menu="menu-2"><i class="far fa-bell font-12"></i></a>
        </div>
        <div id="menu-1" class="menu-wrapper menu-light menu-sidebar-left menu-large">
            <div class="menu-scroll">
                <div class="menu-socials">
                    <a href="https://www.facebook.com/enabled.labs" class="font-12"><i class="fab facebook-color fa-facebook-f"></i></a>
                    <a href="https://twitter.com/iEnabled" class="font-12"><i class="fab twitter-color fa-twitter"></i></a>
                    <a href="https://plus.google.com/u/1/105775801838187143320" class="font-12"><i class="fab google-color fa-google-plus-g"></i></a>
                    <a href="tel:+(1234)567890" class="font-13"><i class="fa color-green-dark fa-phone"></i></a>
                    <a href="page-contact.html" class="font-13"><i class="fa color-blue2-light fa-envelope"></i></a>
                    <a href="#" class="font-14 close-menu"><i class="fa color-red-dark fa-times"></i></a>
                </div>
                <a href="index.html" class="menu-logo"></a>
                <div class="dark-mode-toggle"><a href="#" class="toggle-2 toggle-trigger"><strong><u></u>Dark Mode</strong><i class="bg-green-dark"></i><span></span><i class="bg-gray-light"></i></a></div>
                <div class="dark-menu-toggle"><a href="#" class="toggle-2 toggle-trigger"><strong><u></u>Dark Menu</strong><i class="bg-green-dark"></i><span></span><i class="bg-gray-light"></i></a></div>
                <div class="menu">
                    
                    <a class="menu-item active-item" href="dashboard.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Home</strong></a>
                    <a class="menu-item " href="sermons.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Sermon</strong></a>
                    <a class="menu-item " href="events.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Event</strong></a>
                    <a class="menu-item " href="gallery.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Gallery</strong></a>
                    <a class="menu-item " href="prayer.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Prayer Request</strong></a>
                    <a class="menu-item " href="contact.php"><i class="font-15 fa color-night-light fa-home"></i><strong>Contact Us</strong></a>
                    <a class="menu-item close-menu " href="#"><i class="font-14 fa color-orange-dark fa-times"></i><strong>Close</strong></a>
                    <a class="menu-item close-menu " href="#"><i class="font-14 fa  fa-info-circle"></i><strong>Privacy Policy</strong></a>
                </div>
            </div>
        </div>
        <div id="page-content" class="page-content">
                <div id="page-content-scroll" class="header-clear-larger">
                    <div data-article-card="article-1" class="article-card bg-white no-border">
                        <a data-deploy-card="article-1" href="#" class="article-header">
                            <span class="article-overlay"></span>
                            <span class="article-image preload-image" data-src="images/pictures/christmas.jpg"></span>
                            <span class="article-category color-white bg-border-light uppercase">Christmas Event</span>
                            <span class="article-author color-gray-light"><i class="fa fa-user opacity-50"></i>Minhas Camp</span>
                            <span class="article-time color-gray-light">24th December 2019<i class="far fa-clock opacity-50"></i></span>
                        </a>
                        <div class="article-content">
                            <h1 class="article-title half-top">Christamas Eve</h1>
                            
                        </div>
                        <div class="article-content-hidden">
                            
                            <div class="decoration bg-black opacity-10"></div>
                            
                            <p class="full-bottom">
                                Gather the fam and catch Super Santa and the Superdudes in the flesh. Expect plenty of music, singing, and dancing. Get into the Christmas spirit and snap a selfie with Santa as well as the Superdudes. And if you do spot some familiar faces, they're most likely former Hi-5's Stevie Nicholson and Tanika Anderson. We did tell you Santa is coming to town, so don't pout if you are in the naughty list for not coming. 
                            </p>
                            
                            
                            
                            <div class="clear"></div>
                            <div class="decoration bg-black opacity-10"></div>
                            
                            
                        </div>
                    </div>
                    <div data-article-card="article-2" class="article-card bg-white no-border">
                        <a data-deploy-card="article-2" href="#" class="article-header">
                            <span class="article-overlay"></span>
                            <span class="article-image preload-image" data-src="images/pictures/santa.jpg"></span>
                            <span class="article-category color-white bg-border-light uppercase">Christmas Event</span>
                            <span class="article-author color-gray-light"><i class="fa fa-user opacity-50"></i>Minhas Camp</span>
                            <span class="article-time color-gray-light">24th December 2019<i class="far fa-clock opacity-50"></i></span>
                        </a>
                        <div class="article-content">
                            <h1 class="article-title half-top">The Super Santa Christmas Show</h1>
                            
                        </div>
                        <div class="article-content-hidden">
                            
                            <div class="decoration bg-black opacity-10"></div>
                            
                            <p class="full-bottom">
                                Gather the fam and catch Super Santa and the Superdudes in the flesh. Expect plenty of music, singing, and dancing. Get into the Christmas spirit and snap a selfie with Santa as well as the Superdudes. And if you do spot some familiar faces, they're most likely former Hi-5's Stevie Nicholson and Tanika Anderson. We did tell you Santa is coming to town, so don't pout if you are in the naughty list for not coming. 
                            </p>
                            
                            
                            
                            <div class="clear"></div>
                            <div class="decoration bg-black opacity-10"></div>
                            
                            
                        </div>
                    </div>
                    <!-- <div data-article-card="article-2" class="article-card bg-white no-border">
                        <a data-deploy-card="article-2" href="#" class="article-header">
                            <span class="article-overlay"></span>
                            <span class="article-image preload-image" data-src="images/pictures/1.jpg"></span>
                            <span class="article-category color-white bg-green-dark bg-border-light uppercase">Computers</span>
                            <span class="article-author color-gray-light"><i class="fa fa-user opacity-50"></i>Enabled</span>
                            <span class="article-time color-gray-light">15th September 2019<i class="far fa-clock opacity-50"></i></span>
                        </a>
                        <div class="article-content">
                            <h1 class="article-title half-top">Typewritters are going back in fashion, and they are gorgeous.</h1>
                            <p class="half-bottom"> In aliquet elit erat, at posuere eraterat sagittis lobortis. Integer sit amet nulla sit amet mauris finibus gravida nec vitae augue euismod scelerisque a neque </p>
                        </div>
                        <div class="article-content-hidden">
                            <div class="social-icons full-top half-bottom">
                                <a href="#" class="icon icon-xs icon-square facebook-bg"><i class="fab fa-facebook-f"></i></a>
                                <a href="#" class="icon icon-xs icon-square twitter-bg"><i class="fab fa-twitter"></i></a>
                                <a href="#" class="icon icon-xs icon-square google-bg"><i class="fab fa-google-plus-g"></i></a>
                                <a href="#" class="icon icon-xs icon-square pinterest-bg"><i class="fab fa-pinterest-p"></i></a>
                            </div>
                            <div class="decoration bg-black opacity-10"></div>
                            <h5 class="bold color-black">This is an image gallery</h5>
                            <p class="full-bottom">
                                This page is not WordPress, it's an HTML website. Yes, that's right. but you can convert it to WordPress yourself
                                or hire an expert from Envato Studio.
                            </p>
                            <a href="images/pictures/6.jpg" class="show-gallery polaroid-effect full-bottom" title="Beautiful Camera">
                                <img src="images/empty.png" data-src="images/pictures/6w.jpg" class="preload-image responsive-image no-bottom">
                                <p class="no-bottom center-text">Polaroid Effect Image</p>
                            </a>
                            <div class="one-half">
                                <h4 class="ultrabold small-bottom color-black">1/1</h4>
                                <img data-src="images/pictures/isolated/1.jpg" src="images/empty.png" class="preload-image responsive-image" alt="img">
                                <p>This is half a column.</p>
                            </div>
                            <div class="one-half last-column">
                                <h4 class="ultrabold small-bottom color-black">1/2</h4>
                                <img data-src="images/pictures/isolated/2.jpg" src="images/empty.png" class="preload-image responsive-image" alt="img">
                                <p>This is half a column.</p>
                            </div>
                            <div class="clear"></div>
                            <div class="decoration bg-black opacity-10"></div>
                            <div class="one-third">
                                <h4 class="ultrabold half-bottom color-black">1/3</h4>
                                <img data-src="images/pictures/isolated/3.jpg" src="images/empty.png" class="preload-image responsive-image" alt="img">
                                <p>This is a third of a column.</p>
                            </div>
                            <div class="one-third">
                                <h4 class="ultrabold half-bottom color-black">2/3</h4>
                                <img data-src="images/pictures/isolated/4.jpg" src="images/empty.png" class="preload-image responsive-image" alt="img">
                                <p>This is a third of a column.</p>
                            </div>
                            <div class="one-third last-column">
                                <h4 class="ultrabold half-bottom color-black">2/3</h4>
                                <img data-src="images/pictures/isolated/5.jpg" src="images/empty.png" class="preload-image responsive-image" alt="img">
                                <p>This is a third of a column.</p>
                            </div>
                            <div class="clear"></div>
                            <div class="decoration bg-black opacity-10"></div>
                            <h5 class="bold color-black">Lists</h5>
                            <p>All sorts of lists, even the feature page is a list, but classic ones are also necessary an included in our item.</p>
                            <ul class="one-half">
                                <li>Unordered List</li>
                                <li>Unordered List
                                    <ul>
                                        <li>Nested Sublist</li>
                                        <li>Nested Sublist</li>
                                        <li>Nested Sublist</li>
                                    </ul>
                                </li>
                                <li>Unordered List</li>
                                <li>Unordered List</li>
                            </ul>
                            <ol class="one-half last-column">
                                <li>Ordered List</li>
                                <li>Ordered List
                                    <ol>
                                        <li>Nested Sublist</li>
                                        <li>Nested Sublist</li>
                                        <li>Nested Sublist</li>
                                    </ol>
                                </li>
                                <li>Ordered List</li>
                                <li>Ordered List</li>
                            </ol>
                            <div class="clear"></div>
                            <a href="#" class="close-article button button-xs button-full half-bottom button-blue ultrabold uppercase">Back to Articles</a>
                        </div>
                    </div> -->
                     <div class="footer footer-dark">
                    <a href="#" class="footer-logo"><img src="images/logo-light.png" alt="" style="width: 110px;margin-left: auto;margin-right: auto;bottom: 20px;"></a>
                    <p style="margin-top: -20px;">Jesus is good news for everyone.
                        Come and meet our growing community and see what Jesus is doing right here in Minhas Camp.</p>
                    <div class="footer-socials">
                        <a href="#" class="scale-hover icon icon-round no-border icon-xs facebook-bg border-teal-3d"><i class="fab fa-facebook-f"></i></a>
                        <a href="#" class="scale-hover icon icon-round no-border icon-xs twitter-bg"><i class="fab fa-twitter"></i></a>
                        <a href="#" class="scale-hover icon icon-round no-border icon-xs google-bg"><i class="fab fa-google-plus-g"></i></a>
                        <a href="#" class="scale-hover icon icon-round no-border icon-xs phone-bg"><i class="fa fa-phone"></i></a>
                        <a href="#" data-deploy-menu="menu-share" class="scale-hover icon icon-round no-border icon-xs bg-teal-dark"><i class="fa fa-retweet font-15"></i></a>
                        <a href="#" class="scale-hover icon icon-round no-border icon-xs back-to-top bg-blue-dark"><i class="fa fa-angle-up font-16"></i></a>
                    </div>
                    <p class="copyright-text">Copyright &copy; Blessing <span id="copyright-year">2019</span>. All Rights Reserved.</p>
                </div>
            </div>
        </div>
        <a href="#" class="back-to-top-badge back-to-top-small"><i class="fa fa-angle-up"></i>Back to Top</a>
    </div>
    <script type="text/javascript" src="scripts/jquery.js"></script>
    <script type="text/javascript" src="scripts/custom.js"></script>
    <script type="text/javascript" src="scripts/plugins.js"></script>
</body>
</html>